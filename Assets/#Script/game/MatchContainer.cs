/*
    Copyright 2014 ColaBearStudio. Choi Ethan.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using cbs.pang.basic.color;
using cbs.pang.game.model;

namespace cbs.pang.game
{
    public partial class MatchContainer : MonoBehaviour
    {

        #region -------Public Field-------
        public static MatchContainer container;
        public Block[,] blockList;
        public GameObject blockPrefab;
        public Camera camera; // 매치화면을 보여줄 카메라 컴포넌트.
        public int width; // 가로 블록 개수
        public int height; // 세로 블럭 개수
        public float pad; // 카메라 여백 값.

        #endregion

        #region -------Private Field-------
        // playable
        bool enable = false;

        // block data 
        bool firstSelect = false;
        int selectBlockType = -1;
        public Dictionary<int , BlockData> selectPosDic;
        public Dictionary<int , int> selectTypeDic;
        public int selectCount = 0;
                
        // block sprite
        Sprite[] blockSpr;
        Dictionary<string, Sprite> blockSprDic = new Dictionary<string, Sprite>();

        #endregion

        #region -------Default Method-------

        void Start()
        {
            container = this;
            GameData.gameData = new GameData();

            camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

            blockList = new Block[width, height];
            float offset = Values.Block_Scale / 2;

            transform.position = new Vector2(-(width / 2f) + offset, -(height / 2f) + offset);

            if (Screen.width > Screen.height)
            {
                float hRatio = 0.5f; // height / height = 1
                float wRatio = (float)Screen.width / Screen.height;

                float hViewSize = height / (hRatio * 2);
                float wViewSize = width / (wRatio * 2);

                Debug.Log("hviewSize :" + hViewSize);
                Debug.Log("wViewSize :" + wViewSize);

                camera.orthographicSize = hViewSize >= wViewSize ? hViewSize : wViewSize;
                camera.orthographicSize += pad;
            }
            else if (Screen.width < Screen.height)
            {
                float hRatio = (float)Screen.height / Screen.width;
                float wRatio = 0.5f;

                float hViewSize = width / (wRatio * 2);
                float wViewSize = height / (hRatio * 2);

                Debug.Log("hviewSize :" + hViewSize);
                Debug.Log("wViewSize :" + wViewSize);

                camera.orthographicSize = hViewSize >= wViewSize ? hViewSize : wViewSize;
                camera.orthographicSize += pad;
            }
            else
                GetComponent<Camera>().orthographicSize = width / 2f + pad;




            // block init
            InitializeBlockData();
            InitializeBlock();

            GameData.gameData.InitializeGameData();
            
            AllBlockPositionReset();
            AllBlockSpawn();
            AllBlockColorSet();
            StartCoroutine(matchDownCoroutine());
            
            enable = true;

        }


        void Update()
        {
            if (enable)
            {

                // 클릭 앤 드래그 처리
                if (Input.GetMouseButton(0))
                {
                    Vector2 pos = ScreenToWorldPoint(Input.mousePosition);

                    int x = (int)(pos.x + width / 2f);
                    int y = (int)(pos.y + height / 2f);

                    if (x >= width || y >= height) return;

                    if (!firstSelect)
                    {

                        if (pos.x > -(width / 2f) && pos.x < width / 2f && pos.y > -(height / 2f) && pos.y < height / 2f)
                        {
                            if (!checkClickAble(x)) return;
                            
                            setBlockData(x, y, blockList[x, y]._BlockType);                            
                            clickedEvent(x, y);

                            // 선택한 블럭 종류만 잘 보이게
                            OtherBlockSetAlpha(128);

                        }
                    }
                    else
                    {

                        int lx = selectPosDic[selectCount - 1].x;
                        int ly = selectPosDic[selectCount - 1].y;

                        int lpx = selectPosDic.Count > 1 ? selectPosDic[selectCount - 2].x : 0;
                        int lpy = selectPosDic.Count > 1 ? selectPosDic[selectCount - 2].y : 0;
                        
                        int cx = Math.Abs(lx - x);
                        int cy = Math.Abs(ly - y);

                        // 인접한 블럭일때 체크         
                        // 1. x,y 합쳐서 1 혹은 2의 변화
                        // 2. x,y 각자 1 초과하는 변화는 X
                        if (cx + cy > 0 && cx < 2 && cy < 2)
                        {   
                            if(selectPosDic.Count > 1 && x == lpx && y == lpy)
                            {
                                DeletelastBlockData();                                
                                blockList[lx, ly].DeSelect();
                            }
                            // 블록 타입이 하트, 방패, 별은 독자적으로 링크
                            else if(selectBlockType == (int)Block.Type.HEART || selectBlockType == (int)Block.Type.SHIELD || selectBlockType == (int)Block.Type.STAR)
                            {
                                if (selectBlockType == blockList[x, y]._BlockType) 
                                {
                                    if (blockList[x, y]._State != Block.State.SELECT)
                                    {
                                        setBlockData(x, y, blockList[x, y]._BlockType);
                                        clickedEvent(x, y);
                                    }
                                }
                            }
                            // 블록 타입이 칼과 몹일 경우엔 같이 링크 가능
                            else if (selectBlockType == (int)Block.Type.SWORD || selectBlockType == (int)Block.Type.MOB)
                            {
                                if (blockList[x, y]._BlockType == (int)Block.Type.SWORD || blockList[x, y]._BlockType == (int)Block.Type.MOB)
                                {
                                    if (blockList[x, y]._State != Block.State.SELECT)
                                    {
                                        setBlockData(x, y, blockList[x, y]._BlockType);                                        
                                        clickedEvent(x, y);
                                    }
                                }
                            }
                            //Block.Type.SWORD
                        }
                    }
                }

                // 릴리즈 처리
                if (Input.GetMouseButtonUp(0))
                {
                    completeSelect();                    
                }

            }
            // Input Click -> Get Position -> Block Enable Check -> First? Second?
        }


        #endregion

        #region -------Public Method-------

        public void setBlockData(int x , int y , int type)
        {
            if(selectBlockType == -1)
            {
                selectBlockType = type;
                firstSelect = true;
            }
            selectPosDic.Add(selectCount, new BlockData(x, y));
            selectTypeDic.Add(selectCount, blockList[x, y]._BlockType);
            selectCount++;
        }

        public void DeletelastBlockData()
        {
            selectPosDic.Remove(selectCount - 1);
            selectTypeDic.Remove(selectCount - 1);
            selectCount--;
        }
        
        public void InitializeBlockData()
        {
            selectPosDic = new Dictionary<int, BlockData>();
            selectTypeDic = new Dictionary<int, int>();
            selectCount = 0;
        }

        public void resetBlockData()
        {
            selectPosDic.Clear();
            selectTypeDic.Clear();
            selectBlockType = -1;
            selectCount = 0;
        }

        public void InitializeBlock()
        {
            for (int x = 0; x < width; ++x)
            {
                //TODO : check match and reset color
                for (int y = 0; y < height; ++y)
                {
                    
                    GameObject go = Instantiate(blockPrefab) as GameObject;
                    go.transform.parent = transform;
                    go.name = "block";
                    go.transform.localPosition = new Vector2(x, y);

                    Block block = go.GetComponent<Block>();
                    blockList[x, y] = block;

                    block.SetData(x, y);
                }
            }

            // resources load
            
            blockSpr = Resources.LoadAll<Sprite>("");

            foreach (Sprite a in blockSpr)
            {
                blockSprDic.Add(a.name, a);
            }
            blockSpr = null;

        }

        
        public void OtherBlockSetAlpha(int alpha)
        {
            foreach (Block block in blockList)
            {
                if(block._BlockType != selectBlockType)
                {

                    Color color = block._Render.color;
                    color.a = ((float)(alpha * 100 / 255)) / 100;
                    block._Render.color = color;
                }

            }
        }

        public void AllBlockSetAlpha(int alpha)
        {
            foreach (Block block in blockList)
            {
                Color color = block._Render.color;
                color.a = ((float)(alpha * 100 / 255)) / 100;                
                block._Render.color = color;
            }

        }

        
        public void AllBlockSpawn()
        {
            if (blockList == null) return;

            foreach (Block block in blockList)
                block.Spawn();
        }

        public void AllBlockColorSet()
        {
            if (blockList == null) return;

            int maxLength = (int)Block.Type.SWORD + 1;
            // 최초 블럭 세팅
            foreach (Block block in blockList)
            {
                block.SetRandomColor(maxLength);
            }

            while (checkMatchAll() == false)
            {
                foreach (Block block in blockList)
                {
                    block.SetRandomColor(maxLength);
                }
            }
            
        }

        public void ChangeSprite(Block block)
        {
            if (blockSprDic == null) return;

            switch(block._BlockType)
            {
                case (int)Block.Type.HEART: block._Render.sprite = blockSprDic["heart"]; break;
                case (int)Block.Type.SHIELD: block._Render.sprite = blockSprDic["shield"]; break;
                case (int)Block.Type.STAR: block._Render.sprite = blockSprDic["star"]; break;
                case (int)Block.Type.SWORD: block._Render.sprite = blockSprDic["sword"]; break;
                case (int)Block.Type.MOB: block._Render.sprite = blockSprDic["mob"]; break;
                
            }
            
        }

        public void FinishedMoveEvent(Block block)
        {
            if (findMatchAtBlock(block))
            {
                matchProcess();
                //emptyDown();
            }
        }
        public void AllBlockPositionReset()
        {
            for (int x = 0; x < width; ++x)
                for (int y = 0; y < height; ++y) blockList[x, y].SetPosition(x, y);
        }

        #endregion

        #region -------Private Method-------

        void completeSelect()
        {

            GameData.gameData.UpdateGameData();

            firstSelect = false;
            AllBlockSetAlpha(255);
            matchProcess();
            resetBlockData();
        }

        /// <summary>
        /// checked block click able at line(x)
        /// </summary>
        /// <param name="x">line</param>
        bool checkClickAble(int x)
        {
            for (int y = 0; y < height; ++y)
                if (blockList[x, y]._State == Block.State.MATCH_WAIT || blockList[x, y]._State == Block.State.MOVE) return false;

            return true;
        }

        void matchProcess()
        {

            int blockCnt = 0;
            foreach (Block block in blockList)
            {
                if (block._State == Block.State.MATCH_END) continue;
                if (block._State == Block.State.SELECT) blockCnt++;
            }

            if (blockCnt >= 3)
            {
                foreach (Block block in blockList)
                {

                    if (block._State == Block.State.MATCH_END) continue;
                    if (block._State == Block.State.SELECT) block.Match();
                }

            }
            else
            {
                foreach (Block block in blockList)
                {

                    if (block._State == Block.State.MATCH_END) continue;
                    if (block._State == Block.State.SELECT) block.DeSelect();
                }
            }
        }

        IEnumerator matchCoroutine()
        {
            
            yield return null;
        }

        void clickedEvent(int x, int y)
        {
            if (blockList[x, y]._State == Block.State.MOVE || blockList[x, y]._State == Block.State.MATCH_END) return;
                        
            blockList[x, y].Select();
            
        }

        Vector2 ScreenToWorldPoint(Vector2 pos)
        {
            return Camera.main.ScreenToWorldPoint(pos);
        }

        void refresh()
        {

        }


        void swap(Block a, Block b)
        {
            blockList[a._X, a._Y] = b;
            blockList[b._X, b._Y] = a;

            a.SetData();
            b.SetData();
        }

        void swap(int ax, int ay, int bx, int by)
        {
            Block tmp = blockList[ax, ay];
            blockList[ax, ay] = blockList[bx, by];
            blockList[bx, by] = tmp;

        }

        /// <summary>
        /// match blocks move
        /// </summary>
        void matchDown()
        {
            for (int x = 0; x < width; ++x)
            {
                List<Block> stack = new List<Block>();

                for (int y = 0; y < height; ++y)
                {
                    Block block = blockList[x, y];

                    if (block._State == Block.State.MATCH_END)
                    {
                        if (!stack.Contains(block))
                        {
                            stack.Add(block);
                            block._LocalPosition = new Vector2(x, height);
                        }
                    }

                    else if (stack.Count != 0)
                    {
                        swap(x, y, x, y - stack.Count);
                        block.Move(x, y - stack.Count);
                    }
                }

                for (int i = 0; i < stack.Count; ++i)
                {
                    Block block = blockList[x, height - i - 1];

                    block.On();
                    block.SetRandomColor((int)Block.Type.MOB + 1);
                    block.Move(x, height - i - 1);
                }
            }
        }
        /// <summary>
        /// Is Recursive Method 
        /// Find match end blocks and swap,move         
        /// </summary>
        /// <returns></returns>
        IEnumerator matchDownCoroutine()
        {

            
            for (int x = 0; x < width; ++x)
            {
                List<Block> stack = new List<Block>();

                for (int y = 0; y < height; ++y)
                {
                    Block block = blockList[x, y];

                    if (block._State == Block.State.MATCH_END)
                    {
                        if (!stack.Contains(block))
                        {
                            stack.Add(block);
                            block._LocalPosition = new Vector2(x, height);
                        }
                    }

                    else if (stack.Count != 0)
                    {
                        swap(x, y, x, y - stack.Count);
                        block.Move(x, y - stack.Count);
                        
                    }
                }

                for (int i = 0; i < stack.Count; ++i)
                {
                    Block block = blockList[x, height - i - 1];

                    block.On();
                    block.SetRandomColor((int)Block.Type.MOB + 1);
                    block.Move(x, height - i - 1);
                    
                }
            }

            

            yield return null;

            StartCoroutine(matchDownCoroutine());

        }

        /// <summary>
        /// call count only one
        /// </summary>
        void checkCanMatch()
        {
            StopCoroutine("checkCanMatchCoroutine");
            StartCoroutine("checkCanMatchCoroutine");
        }

        IEnumerator checkCanMatchCoroutine()
        {
            while (isMoving()) yield return null;


            AllBlockColorSet();
        }

        
        bool isMoving()
        {
            foreach (Block block in blockList)
            {
                if (block._State == Block.State.MOVE) return true;
            }

            return false;
        }

        //List<MatchData> matchList = new List<MatchData>();
        bool findMatchAtIDLE()
        {
            bool match = false;
            for (int x = 0; x < width; ++x)
            {
                for (int y = 0; y < height; ++y)
                {

                }
            }
            return match;
        }


        bool findMatchAtBlock(Block block)
        {
            if (block._State == Block.State.MATCH_END) return false;
            bool match = false;
            

            return match;
        }

        bool CompareColor(Block block, int fx, int fy, int sx, int sy)
        {
            //Colors color = block.CurrentColor;
            int type = block._BlockType;
            int x = block._X;
            int y = block._Y;

            int x1 = x + fx;
            int y1 = y + fy;
            int x2 = x + sx;
            int y2 = y + sy;

            if (x1 < 0 || x1 > width - 1 || x2 < 0 || x2 > width - 1 || y1 < 0 || y1 > height - 1 || y2 < 0 || y2 > height - 1) return false;

            return type == blockList[x1, y1]._BlockType && type == blockList[x2, y2]._BlockType;
        }

        bool CompareColor(int type, int fx, int fy, int sx, int sy)
        {
            return type == blockList[fx, fy]._BlockType && type == blockList[sx, sy]._BlockType;
            //return color == blockList[fx, fy].CurrentColor && color == blockList[sx, sy].CurrentColor;
        }
        /// <summary>
        /// Call at Blocks swap Vertical 
        /// </summary>
        

        bool CompareColorInHorizontal(Block block)
        {
            int x = block._X;
            int y = block._Y;
            
            int type = block._BlockType;

            return (CompareColor(type, x - 1, y) && CompareColor(type, x + 1, y));
        }

        /// <summary>
        /// Call at Blocks swap Horizontal
        /// </summary>
        /// 
        bool CompareColorInVertical(Block block)
        {
            int x = block._X;
            int y = block._Y;
            
            int type = block._BlockType;

            return (CompareColor(type, x, y - 1) && CompareColor(type, x, y + 1));
        }

        bool CompareColor(Color color, int fx, int fy, int sx, int sy)
        {
            return false;
        }

        bool CompareColor(int a, int b)
        {
            return a == b;
        }
        bool CompareColor(int a, Block b)
        {
            return a == b._BlockType;
        }

        bool CompareColor(int a, int x, int y)
        {
            if (x < 0 || y < 0 || x >= width || y >= height) return false;
            return a == blockList[x, y]._BlockType;
        }

        bool checkMatchAll()
        {
            int matchCnt = 0;

            foreach (Block block in blockList)
            {
                if (checkMatch(block._X, block._Y)) matchCnt++;
            }

            if (matchCnt > 0) return true;

            return false;
        }


        /// <summary>
        /// Only Check Match at Position
        /// </summary>
        /// <param name="x">target index x</param>
        /// <param name="y">target index y</param>
        /// <returns></returns>
        /// 
        bool checkMatch(int x, int y)
        {
            int blockType = blockList[x, y]._BlockType;
            int matchCnt = 0;
            int tempType = -1;

            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    // 범위 밖
                    if (x + i < 0 || x + i > 5) continue;
                    if (y + j < 0 || y + j > 5) continue;

                    // 기준 블럭
                    if (i == 0 && j == 0) continue;

                    tempType = blockList[x + i, y + j]._BlockType;

                    if (tempType == blockType) matchCnt++;

                }
            }

            if(matchCnt >= 2) return true;
            
            return false;
        }
        bool checkMatch(Block block)
        {
            Colors pColor = block.CurrentColor;
            int x = block._X;
            int y = block._Y;


            //left
            if (x > 1 && pColor == blockList[x - 1, y].CurrentColor && pColor == blockList[x - 2, y].CurrentColor) return true;
            //right
            else if (x < width - 2 && pColor == blockList[x + 1, y].CurrentColor && pColor == blockList[x + 2, y].CurrentColor) return true;
            //top
            else if (y < height - 2 && pColor == blockList[x, y + 1].CurrentColor && pColor == blockList[x, y + 2].CurrentColor) return true;
            //bottom
            else if (y > 1 && pColor == blockList[x, y - 1].CurrentColor && pColor == blockList[x, y - 2].CurrentColor) return true;

            // 

            int matchCnt = 0;
            for(int i = -1 ; i < 1 ; i++)
            {
                for (int j = -1; j < 1; j++)
                {
                    Debug.Log("i , j : " + i + "/" + j);
                }
            }



            return false;
        }


        #endregion

        #region -------Property-------


        #endregion

    }
}
