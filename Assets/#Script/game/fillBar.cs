﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using cbs.pang.game.model;

public class fillBar : MonoBehaviour
{
    // Unity UI References
    Slider slider;
    Text displayText;

    private float currentValue = 0f;
    public float CurrentValue
    {
        get
        {
            return currentValue;
        }
        set
        {
            currentValue = value;
            slider.value = currentValue;

            

        }
    }

    public string CurrentText
    {
        get
        {
            return displayText.text;
        }
        set
        {
            displayText.text = value;
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        
        slider = GetComponent<Slider>();
        displayText = this.transform.Find("Text").GetComponent<Text>();

        CurrentValue = 0f;        
    }

    // Update is called once per frame
    void Update()
    {
        //CurrentValue += 0.0043f;
        //CurrentValue = 
    }
}
