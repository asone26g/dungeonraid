﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace cbs.pang.game.model
{
    public class GameData
    {
        public static GameData gameData;

        // game data
        public int turnCount;

        public int hpGage, hpMax;  // hp가 0이 되면 게임오버
        public int mobExpGage, mobExpMax; // 해골 처치시 경험치 획득
        public int starGage, starMax; // 일정 스타 이상 획득시 card 획득        
        public int shieldExpGage, shieldExpMax; // 방어력 경험치

        public int playerAtk;
        public int mobAtk;
        public int playerDef;
        public int playerDefMax;



        fillBar HpBar;
        fillBar StarBar;
        fillBar ShieldBar;
        fillBar ExpBar;
        GameObject playerAtkUI;
        GameObject mobAtkUI;
        GameObject playerDefUI;

        void Awake()
        {
            gameData = this;
        }

        // Start is called before the first frame update
        void Start()
        {
            
            
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        public void InitializeGameData()
        {
            turnCount = 0;

            hpMax = 50;
            hpGage = hpMax;

            starMax = 50;
            starGage = 0;

            mobExpMax = 50;
            mobExpGage = 0;

            
            shieldExpMax = 50;
            shieldExpGage = 0;

            HpBar = GameObject.Find("HpBar").GetComponent<fillBar>();
            StarBar = GameObject.Find("StarBar").GetComponent<fillBar>();
            ExpBar = GameObject.Find("ExpBar").GetComponent<fillBar>();
            ShieldBar = GameObject.Find("ShieldBar").GetComponent<fillBar>();

            playerAtk = 3;
            mobAtk = 2;
            playerDefMax = 4;
            playerDef = playerDefMax;

            playerAtkUI = GameObject.Find("playerAtk");
            playerDefUI = GameObject.Find("playerDef");
            mobAtkUI = GameObject.Find("mobAtk");


            UpdateGage();
            updateStatUI();

        }

        public void updateStatUI()
        {
            
            playerAtkUI.GetComponent<Text>().text = "ATK : " + playerAtk.ToString();
            playerDefUI.GetComponent<Text>().text = "DEF : " + playerDef.ToString() + "/" + playerDefMax.ToString();
            mobAtkUI.GetComponent<Text>().text = "Enemy ATK : " + mobAtk.ToString();

        }

        public void UpdateGage()
        {

            HpBar.CurrentValue = (float)hpGage / (float)hpMax;
            HpBar.CurrentText = "HP : " + hpGage.ToString() + " / " + hpMax.ToString();
           
            StarBar.CurrentValue = (float)starGage / (float)starMax;
            StarBar.CurrentText = "Star : " + starGage.ToString() + " / " + starMax.ToString();
           
            ExpBar.CurrentValue = (float)mobExpGage / (float)mobExpMax;
            ExpBar.CurrentText = "Exp : " + mobExpGage.ToString() + " / " + mobExpMax.ToString();
            
            ShieldBar.CurrentValue = (float)shieldExpGage / (float)shieldExpMax;
            ShieldBar.CurrentText = "Shield : " + shieldExpGage.ToString() + " / " + shieldExpMax.ToString();

        }

        public void UpdateGameData()
        {
            
            int hpCnt = 0;
            int shieldCnt = 0;
            int starCnt = 0;
            int swordCnt = 0;
            int mobCnt = 0;
            int bonusCnt = 0;
            int type = -1;

            turnCount++;

            foreach (var a in MatchContainer.container.selectTypeDic)
            {
                type = a.Value;

                switch (type)
                {
                    case (int)Block.Type.HEART: hpCnt++; break;
                    case (int)Block.Type.SHIELD: shieldCnt++; break;
                    case (int)Block.Type.STAR: starCnt++; break;
                    case (int)Block.Type.SWORD: swordCnt++; break;
                    case (int)Block.Type.MOB: mobCnt++; break;
                }

            }

            /*
            Debug.Log("hpCnt : " + hpCnt);
            Debug.Log("shieldCnt : " + shieldCnt);
            Debug.Log("starCnt : " + starCnt);
            Debug.Log("swordCnt : " + swordCnt);
            Debug.Log("mobCnt : " + mobCnt);
            */

            // hp 처리
            if(hpCnt > 0)
            {
                if(hpCnt > 3)
                {
                    bonusCnt = (hpCnt - 3) / 2;
                }
                hpGage += (hpCnt + bonusCnt);
                if(hpGage > hpMax)                
                    hpGage = hpMax;
                

            }

            // shield 처리
            if(shieldCnt > 0)
            {                
                shieldExpGage += shieldCnt;
                if(shieldExpGage > shieldExpMax)                
                    shieldExpGage = shieldExpMax;                

                playerDef += shieldCnt;
                if (playerDef > playerDefMax)                
                    playerDef = playerDefMax;
                
            }
            
            // exp 처리
            if(mobCnt > 0)
            {
                mobExpGage += mobCnt;
                if (mobExpGage > mobExpMax)
                    mobExpGage = mobExpMax;
            }

            // star 처리
            if(starCnt > 0)
            {
                starGage += starCnt;
                if (starGage > starMax)
                    starGage = starMax;

            }

            UpdateGage();

        }


    }
}